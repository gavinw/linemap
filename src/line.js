import utils from "./utils";
import Segment from "./segment";

export default function Line(points, color) {
  this.points = points || [];
  this.segments = [];
  this.milestones = [];
  this.isSelectd = false;
  this.color = "#" + color;
  this.labels = [];
  this.uuid = "";

  for (var i = 1; i < points.length; i++) {
    this.segments.push(new Segment(points[i - 1], points[i]));
  }
}

Line.prototype.addLabel = function(label) {
  label.color = this.color;
  this.labels.push(label);
};

Line.prototype.draw = function(context) {
  context.save();

  var self = this;

  this.drawSegments(context);
  // this.segments.forEach(function(segment) {
  //   segment.draw(context, self);
  // });

  this.milestones.forEach(function(station) {
    station.draw(context, self);
  });

  this.labels.forEach(function(label) {
    label.drawBg(context);
  });

  context.restore();
};

Line.prototype.drawSegments = function(context) {
    context.save();

    context.beginPath();
    context.lineWidth = 4;
    context.strokeStyle = this.isSelectd ? 'red' : this.color;

    this.segments.forEach(function (segment, index) {
        if (index === 0) {
            context.moveTo(segment.startPoint.x, segment.startPoint.y);
        }
        else {
            context.lineTo(segment.endPoint.x, segment.endPoint.y);
        }
    });
    context.stroke();

    context.restore();
};

Line.prototype.contains = function(p) {
  return this.segments.some(function(segment) {
    return segment.contains(p);
  });
};
