import Map from './src/map';
import Line from './src/line';
import Mask from './src/mask';
import Message from './src/message';
import Point from './src/point';
import Segment from './src/segment';
import util from './src/utils';

window.LineMap = {
    Map: Map,
    Line: Line,
    Mask: Mask,
    Message: Message,
    Point: Point,
    Segment: Segment,
    util: util
};

export default { Map, Line, Mask, Message, Point, Segment, util }