import Message from "./message";

/**
 * Normalize the browser animation API across implementations. This requests
 * the browser to schedule a repaint of the window for the next animation frame.
 * Checks for cross-browser support, and, failing to find it, falls back to setTimeout.
 * @param {function}    callback  Function to call when it's time to update your animation for the next repaint.
 * @param {HTMLElement} element   Optional parameter specifying the element that visually bounds the entire animation.
 * @return {number} Animation frame request.
 */
if (!window.requestAnimationFrame) {
  window.requestAnimationFrame =
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    function(callback) {
      return window.setTimeout(callback, 17 /*~ 1000/60*/);
    };
}

/**
 * Cancels an animation frame request.
 * Checks for cross-browser support, falls back to clearTimeout.
 * @param {number}  Animation frame request.
 */
if (!window.cancelRequestAnimationFrame) {
  window.cancelRequestAnimationFrame =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    window.clearTimeout;
}

/* Object that contains our utility functions.
 * Attached to the window object which acts as the global namespace.
 */
var utils = {};

/**
 * Keeps track of the current mouse position, relative to an element.
 * @param {HTMLElement} element
 * @return {object} Contains properties: x, y, event
 */
utils.captureMouse = function(element) {
  var mouse = {
      x: 0,
      y: 0,
      event: null,
      onMove: new Message(),
      onPick: new Message(),
      onScale: new Message()
    },
    body_scrollLeft = document.body.scrollLeft,
    element_scrollLeft = document.documentElement.scrollLeft,
    body_scrollTop = document.body.scrollTop,
    element_scrollTop = document.documentElement.scrollTop,
    offsetLeft = element.offsetLeft,
    offsetTop = element.offsetTop;

  mouse.getLocation = getLocation;

  function getLocation(event) {
    var x, y;

    if (event.pageX || event.pageY) {
      x = event.pageX;
      y = event.pageY;
    } else {
      x = event.clientX + body_scrollLeft + element_scrollLeft;
      y = event.clientY + body_scrollTop + element_scrollTop;
    }
    x -= offsetLeft;
    y -= offsetTop;

    return {
      // x: x,
      // y: y
      x: event.offsetX,
      y: event.offsetY
    };
  }

  element.addEventListener(
    "mousemove",
    function(event) {
      var loc = getLocation(event);
      mouse.moveX = !!mouse.x ? loc.x - mouse.x : 0;
      mouse.moveY = !!mouse.y ? loc.y - mouse.y : 0;
      mouse.x = loc.x;
      mouse.y = loc.y;
      mouse.event = event;
      mouse.onMove.fire(mouse);
    },
    false
  );

  element.addEventListener(
    "mousedown",
    function(event) {
      var loc = getLocation(event);
      mouse.x = loc.x;
      mouse.y = loc.y;
      mouse.moveX = null;
      mouse.moveY = null;
      mouse.isPressed = true;
      mouse.event = event;
    },
    false
  );

  element.addEventListener(
    "mouseup",
    function(event) {
      mouse.isPressed = false;
      mouse.x = null;
      mouse.y = null;
      mouse.event = event;
    },
    false
  );

  element.addEventListener(
    "mouseleave",
    function(event) {
      mouse.isPressed = false;
      mouse.x = null;
      mouse.y = null;
      mouse.moveX = null;
      mouse.moveY = null;
      mouse.event = event;
    },
    false
  );

  element.addEventListener(
    "click",
    function(event) {
      var loc = getLocation(event);
      mouse.x = loc.x;
      mouse.y = loc.y;
      mouse.event = event;
      if (!mouse.moveX && !mouse.moveY) {
        mouse.onPick.fire(mouse);
      }
      mouse.moveX = null;
      mouse.moveY = null;
    },
    false
  );

  element.addEventListener(
    "mousewheel",
    function(event) {
      var loc = getLocation(event);
      var isUp = event.wheelDelta > 0;
      mouse.x = loc.x;
      mouse.y = loc.y;
      mouse.event = event;
      mouse.onScale.fire(mouse, isUp);
    },
    false
  );

  return mouse;
};

/**
 * Keeps track of the current (first) touch position, relative to an element.
 * @param {HTMLElement} element
 * @return {object} Contains properties: x, y, isPressed, event
 */
utils.captureTouch = function(element) {
  var touch = {
      x: null,
      y: null,
      isPressed: false,
      event: null,
      onMove: new Message()
    },
    body_scrollLeft = document.body.scrollLeft,
    element_scrollLeft = document.documentElement.scrollLeft,
    body_scrollTop = document.body.scrollTop,
    element_scrollTop = document.documentElement.scrollTop,
    offsetLeft = element.offsetLeft,
    offsetTop = element.offsetTop;

  element.addEventListener(
    "touchstart",
    function(event) {
      touch.isPressed = true;
      touch.event = event;
    },
    false
  );

  element.addEventListener(
    "touchend",
    function(event) {
      touch.isPressed = false;
      touch.x = null;
      touch.y = null;
      touch.moveX = null;
      touch.moveY = null;
      touch.event = event;
    },
    false
  );

  element.addEventListener(
    "touchmove",
    function(event) {
      var x,
        y,
        touch_event = event.touches[0]; //first touch

      if (touch_event.pageX || touch_event.pageY) {
        x = touch_event.pageX;
        y = touch_event.pageY;
      } else {
        x = touch_event.clientX + body_scrollLeft + element_scrollLeft;
        y = touch_event.clientY + body_scrollTop + element_scrollTop;
      }
      x -= offsetLeft;
      y -= offsetTop;

      touch.moveX = !!touch.x ? x - touch.x : 0;
      touch.moveY = !!touch.y ? y - touch.y : 0;
      touch.x = x;
      touch.y = y;
      touch.event = event;
      touch.onMove.fire(touch);
    },
    false
  );

  return touch;
};

/**
 * Returns a color in the format: '#RRGGBB', or as a hex number if specified.
 * @param {number|string} color
 * @param {boolean=}      toNumber=false  Return color as a hex number.
 * @return {string|number}
 */
utils.parseColor = function(color, toNumber) {
  if (toNumber === true) {
    if (typeof color === "number") {
      return color | 0; //chop off decimal
    }
    if (typeof color === "string" && color[0] === "#") {
      color = color.slice(1);
    }
    return window.parseInt(color, 16);
  } else {
    if (typeof color === "number") {
      color = "#" + ("00000" + (color | 0).toString(16)).substr(-6); //pad
    }
    return color;
  }
};

/**
 * Converts a color to the RGB string format: 'rgb(r,g,b)' or 'rgba(r,g,b,a)'
 * @param {number|string} color
 * @param {number}        alpha
 * @return {string}
 */
utils.colorToRGB = function(color, alpha) {
  //number in octal format or string prefixed with #
  if (typeof color === "string" && color[0] === "#") {
    color = window.parseInt(color.slice(1), 16);
  }
  alpha = alpha === undefined ? 1 : alpha;
  //parse hex values
  var r = (color >> 16) & 0xff,
    g = (color >> 8) & 0xff,
    b = color & 0xff,
    a = alpha < 0 ? 0 : alpha > 1 ? 1 : alpha;
  //only use 'rgba' if needed
  if (a === 1) {
    return "rgb(" + r + "," + g + "," + b + ")";
  } else {
    return "rgba(" + r + "," + g + "," + b + "," + a + ")";
  }
};

/**
 * Determine if a rectangle contains the coordinates (x,y) within it's boundaries.
 * @param {object}  rect  Object with properties: x, y, width, height.
 * @param {number}  x     Coordinate position x.
 * @param {number}  y     Coordinate position y.
 * @return {boolean}
 */
utils.containsPoint = function(rect, x, y) {
  return !(
    x < rect.x ||
    x > rect.x + rect.width ||
    y < rect.y ||
    y > rect.y + rect.height
  );
};

/**
 * Determine if two rectangles overlap.
 * @param {object}  rectA Object with properties: x, y, width, height.
 * @param {object}  rectB Object with properties: x, y, width, height.
 * @return {boolean}
 */
utils.intersects = function(rectA, rectB) {
  return !(
    rectA.x + rectA.width < rectB.x ||
    rectB.x + rectB.width < rectA.x ||
    rectA.y + rectA.height < rectB.y ||
    rectB.y + rectB.height < rectA.y
  );
};

/**
 * Get offset location.
 */
utils.getOffsetXY = function(element, clientXY) {
  var area = element.getBoundingClientRect();
  return {
    x: clientXY.x - area.left,
    y: clientXY.y - area.top
  };
};

/**
 * get pixel ratio.
 * @param context
 * @returns {number}
 */
utils.getPixelRatio = function(context) {
    var backingStore = context.backingStorePixelRatio ||
        context.webkitBackingStorePixelRatio ||
        context.mozBackingStorePixelRatio ||
        context.msBackingStorePixelRatio ||
        context.oBackingStorePixelRatio ||
        context.backingStorePixelRatio || 1;

    return (window.devicePixelRatio || 1) / backingStore;
};


export default utils;
