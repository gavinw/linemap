const path = require('path');

module.exports = {
    mode: 'development',
    devtool: 'inline-source-map',
    entry: './index.js',
    output: {
        filename: 'line-map.js',
        path: path.resolve(__dirname, 'dist')
    }
};