export default function Mask(map){
    this.map = map;
}

Mask.prototype.draw = function (context) {
    context.save();
    var map = this.map;
    context.scale(1 / map.scale.x, 1 / map.scale.y);
    context.translate(-map.translate.x, -map.translate.y);
    context.fillStyle = 'white';
    context.globalAlpha = 0.8;
    context.fillRect(0, 0, map.drawing.width, map.drawing.height);
    context.restore();
};