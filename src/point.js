import Label from "./label";
import { images } from "./images";

export default function Point(x, y, index, text) {
  this.x = x;
  this.y = y;
  this.radius = 4;
  this.index = index;
  this.label = null;
  this.canTransfer = false;
  this.uuid = "";
  this.lines = [];

  if (text) {
    this.label = new Label(this.radius, this.radius, text);
  }
}

Point.prototype.setTransfer = function(value) {
  this.canTransfer = value;

  if (this.label) {
    if (this.canTransfer) {
      this.radius = 8;
    } else {
      this.radius = 4;
    }

    this.label.x = this.radius;
    this.label.y = this.radius;
  }
};

Point.prototype.draw = function(context, state) {
  context.save();

  context.translate(this.x, this.y);
  context.beginPath();
  context.arc(0, 0, this.radius, 0, 2 * Math.PI, false);
  context.strokeStyle = "#ccc";
  // context.lineWidth = 1;
  context.stroke();

  context.fillStyle = "white";
  context.fill();

  if (this.canTransfer) {
    context.drawImage(
      images.transfer,
      -this.radius,
      -this.radius,
      this.radius * 2,
      this.radius * 2
    );
  }

  if (this.label) {
    this.label.draw(context);
  }

  context.restore();
};

Point.prototype.contains = function(p) {
    var area = this.radius * this.radius;

    if(area < 64){
        area = 64;
    }

  return (p.x - this.x) * (p.x - this.x) + (p.y - this.y) * (p.y - this.y) <= area;
};
