window.onload = function() {
  var lines = [];
  var lineMap = new LineMap.Map("drawing", lines);

  function getData() {
    return axios.get("./data/subway-gz.json");
  }

  function getInfo() {
    return axios.get("./data/subway-gz-info.json");
  }

  axios.all([getData(), getInfo()]).then(
    axios.spread(function(res, res1) {
      // 两个请求现在都执行完成
      res.data.l.forEach(function(item) {
        var pos = item.lp[0].split(" ");
        var label = {
          x: pos[0],
          y: pos[1],
          text: item.ln
        };
        var line = {
          uuid: item.ls,
          label: label,
          color: item.cl,
          points: [],
          milestones: []
        };

        item.c.forEach(function(i) {
          var arr = i.split(" ");
          line.points.push({ x: parseInt(arr[0]), y: parseInt(arr[1]) });
        });

        item.st.forEach(function(i, index) {
          var arr = i.p.split(" ");
          line.milestones.push({
            uuid: i.si,
            x: parseInt(arr[0]),
            y: parseInt(arr[1]),
            label: { x: 0, y: 0, text: i.n }
          });
        });

        lines.push(line);
      });

      lineMap.refresh();
    })
  );
};
