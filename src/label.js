export default function Label(x, y, text) {
  this.x = x;
  this.y = y;
  this.text = text;
  this.color = "black";
  this.fontSize = 8;
  this.font = "bold " + this.fontSize + "px Arial";
}

Label.prototype.draw = function(context) {
  context.save();
  context.fillStyle = this.color;
  context.font = this.font;
  context.textAlign = "center";
  context.textBaseline = "top";
  context.fillText(this.text, 0, this.y + 2);
  context.restore();
};

// background text
Label.prototype.drawBg = function(context) {
  context.save();
  context.translate(this.x, this.y);
  context.font = this.font;
  var size = context.measureText(this.text);
  context.fillStyle = this.color;
  context.fillRect(-1, -2, size.width + 2, this.fontSize + 2);
  context.fillStyle = "white";
  context.textAlign = "left";
  context.textBaseline = "top";
  context.fillText(this.text, 0, 0);
  context.restore();
};
