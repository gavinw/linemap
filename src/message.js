export default function Message() {
    this.handlers = [];
}

Message.prototype.register = function (handler) {
    this.handlers.push(handler);
};

Message.prototype.unregister = function (handler) {
    this.handlers = this.handlers.filter(function (item) { return item !== handler });
};

Message.prototype.unregisterAll = function () {
    this.handlers.length = 0;
};

Message.prototype.fire = function () {
    var caller = this;
    var args = arguments;
    this.handlers.forEach(function (handler) {
        handler.apply(caller, args);
    });
};